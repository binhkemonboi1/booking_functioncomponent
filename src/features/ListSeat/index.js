import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { message } from "antd";
import "./css/style.css";
import Seat from './components/Seat';
import ListSeatAPI from "../../service/api/ListSeat";
import TYPE from "../../base/contstant/index";

function ListSeat() {
    const dispatch = useDispatch();
    const countRef = useRef(0);
    const [ListSeat, setList] = useState([]);
    const { name, numberSeats } = useSelector(state => state.FormRegisterReducer);
    const { seats } = useSelector(state => state.ListSeatReducer);
    useEffect(() => {
        const fetchListSeat = async () => {
            const ListSeat = await ListSeatAPI.getAllList();
            setList(ListSeat);
        }
        fetchListSeat();
        return () => {
            let listChooed = document.querySelectorAll('.choosed');
            listChooed.forEach(item => item.classList.remove("choosed"));
            dispatch({ type: TYPE.SEAT, payload: [] });
            dispatch({ type: TYPE.REGISTER, payload: { name: "", numberSeats: 0 } });
            countRef.current = 0;
        }
    }, [])
    const handleConfirm = () => {
        if (name === "" || numberSeats === "") {
            message.error("Bạn phải đăng kí vé trước khi chọn ghế ngồi");
            return;
        }
        dispatch({
            type: TYPE.CONFIRM_OF_POST,
            payload: { name, numberSeats, seats },
        })
        let listChooed = document.querySelectorAll('.choosed');
        listChooed.forEach(item => item.classList.remove("choosed"));
        dispatch({ type: TYPE.SEAT, payload: [] });
        dispatch({ type: TYPE.REGISTER, payload: { name: "", numberSeats: 0 } });
        dispatch({ type: TYPE.CHANGE_VALID, payload: false });
        countRef.current = 0;
        message.success("Book Thành Công");
    }
    const renderFirstRowSeat = (ListSeat) => {
        if (ListSeat[0] === undefined || ListSeat[0].length === 0) return;
        return ListSeat[0].danhSachGhe.map((item, index) => <td className='seat-area' key={"row-0-col-" + index}>{item.soGhe}</td>)
    }
    const renderNextSeat = (list) => {
        return list.map((item, index) => {
            if (item.hang !== "") {
                return (<tr key={"row-" + index} >
                    <td key={`row-0-col-${index + 1}`} className='seat-area'>{item.hang}</td>
                    {
                        item.danhSachGhe.map((ghe, index) =>
                            <td key={`row-${index + 1}-col-${index + 1}`} className={(ghe.daDat) ? "reserved" : null}>
                                <Seat numberSeats={numberSeats} count={countRef} ghe={ghe}></Seat>
                            </td>)
                    }
                </tr>)
            }
        })
    }
    return (
        <>
            <table className='table align-middle table-borderless seat-table'>
                <tbody>
                    <tr><td><i className="bi bi-bar-chart-steps"></i></td>{renderFirstRowSeat(ListSeat)}</tr>
                    {renderNextSeat(ListSeat)}
                </tbody>
            </table>
            <ul className='seat-sub'>
                <li><i className="bi bi-arrow-right-square-fill selected"></i><span>Selected Seat</span></li>
                <li><i className="bi bi-arrow-right-square-fill reserved"></i><span>Reserved Seat</span></li>
                <li><i className="bi bi-arrow-right-square-fill empty"></i><span>Empty Seat</span></li>
            </ul>
            <button onClick={() => handleConfirm()} className='btn-confirm' type='button'>Confirm Selection</button>
        </>
    )
}
export default ListSeat;