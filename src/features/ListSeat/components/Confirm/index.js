import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import "./style.css";
import TYPE from "../../../../base/contstant/index";

function Confirm() {
    const dispatch = useDispatch();
    const { name, numberSeats, seats } = useSelector(state => state.ConfirmReducer);

    useEffect(() => {
        return () => {
            dispatch({
                type: TYPE.CONFIRM_OF_POST,
                payload: { name: "", numberSeats: "", seats: "" },
            });
        }
    }, [])

    const total = useMemo(() => {
        if (seats.length === 0)
            return null;
        let result = seats.reduce((result, item) => result + item.gia, 0);
        return result;
    }, [seats]);

    const locations = useMemo(() => {
        if (seats.length === 0)
            return null;

        return seats.reduce((result, item) => result += item.soGhe + " ", "")
    }, [seats]);

    const renderInforSeats = () => {
        return (<tr>
            <td>{name}</td>
            <td>{(numberSeats === 0) ? null : numberSeats}</td>
            <td>{locations}</td>
            <td>{(total !== null) ? total.toLocaleString() : null}</td>
        </tr>)
    }

    return (
        <table className='table table-infoseats align-middle table-borderless '>
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Seats</th>
                    <th>Number</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                {renderInforSeats()}
            </tbody>
        </table>
    )
}
export default Confirm;