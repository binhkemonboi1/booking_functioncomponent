import TYPE from "../../../../base/contstant/index";

const initialState = {
    isValid: false,
    name: "",
    numberSeats: 0,
}

const FormRegisterReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case TYPE.CHANGE_VALID:
            return { ...state, isValid: payload };
        case TYPE.REGISTER:
            return { ...state, name: payload.name, numberSeats: payload.numberSeats };

        default:
            return state
    }
}

export default FormRegisterReducer;