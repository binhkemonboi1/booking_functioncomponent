import React, { memo, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { message } from "antd";
import "./style.css";
import TYPE from "../../../../base/contstant/index";


function FormRegister() {
    const dispatch = useDispatch();
    const inputNameRef = useRef(Element);
    const inputNumberRef = useRef(Element);
    const [register, setRegister] = useState({ name: "", numberSeats: "" });
    const { isValid } = useSelector(state => state.FormRegisterReducer);
    const number = useRef();
    const handleChangeInput = (e) => {
        const input = e.target;
        setRegister({ ...register, [input.name]: input.value });
    }
    const handleSubmit = () => {
        if (register.name === "" || register.numberSeats === "") {
            message.error("Bạn chưa điền tên hoặc số điện thoại");
            return null;
        }
        dispatch({
            type: TYPE.REGISTER,
            payload: register,
        })
        dispatch({
            type: TYPE.CHANGE_VALID,
            payload: true,
        })
        inputNameRef.current.value = null;
        inputNumberRef.current.value = null;
        number.current = register.numberSeats;
        setRegister({ name: "", numberSeats: "" });
    }
    return (
        <div className='form-resigter'>
            <h4 style={{ height: "40px" }} className='form-title text-center'>
                {(isValid) ? `Please Select your ${number.current} Seats NOW` : "Fill The Required Details Below And Select Your Seats"}
            </h4>
            <form className='form'>
                <div className='form-group'>
                    <input onChange={(e) => handleChangeInput(e)} ref={inputNameRef} name="name" className='form-input' type="text" placeholder='Name...' />
                    <input onChange={(e) => handleChangeInput(e)} ref={inputNumberRef} name='numberSeats' className='form-input' type="number" placeholder='Number of Seat...' />
                </div>
                <button onClick={handleSubmit} className='btn-selecting' type='button'>Start Selecting</button>
            </form>
        </div>
    )
}
export default memo(FormRegister);