import { Routes, Route } from "react-router-dom";
import DefaultLayout from "../../layouts/DefaultLayout";
import TicketPage from "../../pages/Ticket/index";


export default function Router() {
    return (
        <Routes>
            <Route path="/ticket" >
                <Route index element={<DefaultLayout><TicketPage /></DefaultLayout>}></Route>
            </Route>
        </Routes>
    )
}

