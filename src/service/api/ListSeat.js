import axiosClient from "../axios/axiosClient"

class ListSeat {
    getAllList = async () => {
        return await axiosClient.get("/danhsachghe").then(res => res);
    }
}

const ListSeatApi = new ListSeat();
export default ListSeatApi;

