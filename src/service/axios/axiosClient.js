import axios from "axios";

const axiosClient = axios.create({
    baseURL: 'https://64c62b5ec853c26efadb28e8.mockapi.io',
});

axiosClient.interceptors.request.use(function (config) {
    return config;
}, function (error) {
    return Promise.reject(error);
});

axiosClient.interceptors.response.use(function (response) {
    return response.data;
}, function (error) {
    return Promise.reject(error);
});

export default axiosClient;