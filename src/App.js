import { BrowserRouter } from "react-router-dom";
import Router from "./service/router/index";


function App() {
  return (
    <BrowserRouter>
      <Router></Router>
    </BrowserRouter>
  );
}

export default App;
