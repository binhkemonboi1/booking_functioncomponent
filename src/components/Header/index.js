import React from 'react';
import "./style.css";
import { NavLink } from 'react-router-dom';

export default function Header() {
    return (
        <header>
            <nav className="navbar navbar-expand-lg">
                <div className="container-fluid">
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <div className="navbar-nav me-auto mb-2 mb-lg-0 mx-auto nav-item">
                            <NavLink className="nav-link" to="/Ticket">GALAXY CINEMA</NavLink>
                        </div>
                    </div>
                </div>
            </nav>
        </header >
    )
}

